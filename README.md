# g2z-sandbox

This is a dummy repo, used by [gitlab2zenodo](https://gitlab.com/sbeniamine/gitlab2zenodo) to check that getting metadata and uploading to zenodo (in the sandbox) works.